Source: stream-lib
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Andrius Merkys <merkys@debian.org>,
Build-Depends:
 debhelper (>= 12),
 default-jdk,
 maven-debian-helper (>= 2.1),
Build-Depends-Indep:
 junit4 (>= 4.12),
 libcolt-free-java,
 libcommons-codec-java (>= 1.11),
 libfastutil-java,
 libguava-java,
 libslf4j-java (>= 1.7.25),
Standards-Version: 4.4.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/java-team/stream-lib.git
Vcs-Browser: https://salsa.debian.org/java-team/stream-lib
Homepage: https://github.com/addthis/stream-lib

Package: libstream-java
Architecture: all
Depends:
 ${maven:Depends},
 ${misc:Depends},
Suggests:
 ${maven:OptionalDepends},
Multi-Arch: foreign
Description: library for summarizing data in streams
 A Java library for summarizing data in streams for which it is infeasible to
 store all events. More specifically, there are classes for estimating:
 cardinality (i.e. counting things); set membership; top-k elements and
 frequency. One particularly useful feature is that cardinality estimators with
 compatible configurations may be safely merged.
